#include <iostream>
#include <ctime>
using namespace std;

int main(){
  const int width = 5, height = 5; // ширина и высота матрицы
  float arrow[height][width];
  srand(time(NULL));

  for(int h = 0; h < height; h++){ // генерирует и печатает матрицу
    cout << "[";
    for(int w = 0; w < width; w++){
      arrow[h][w] = (rand() % 55 - 25) / 5.;
      cout.width(4); // выравнивает печать матрицы
      cout << arrow[h][w] << " ";
    }
    cout << "]\n";
  }

  float min = arrow[0][0], count[width], minIndex, maxColumn[width];

  cout << "Count column\n[";
  for(int w = 0; w < width; w++){
    count[w] = 0; // обнуляет элемент
    maxColumn[w] = arrow[w][0]; // обнуляет элемент
    for(int h = 0; h < height; h++){
      count[w] += arrow[h][w]; // считает сумму элементов колонки
      if(min > arrow[w][h]){ // ищет минимальный элемент и его индекс
        min = arrow[w][h];
        minIndex = h + width * w; // индекс минимального элемента при счете слева на право в строковом представлении массива
      }
      if(maxColumn[w] < arrow[h][w]){ // ищет максимальный элемент колонки
        maxColumn[w] = arrow[h][w];
      }
    }
    cout << count[w] << " ";
  }
  cout << "]\n";

  cout << "MaxColumn\n["; // печатает максимальные элементы колонок
  for(int i; i < width; i++){
    cout << maxColumn[i] << " ";
  }
  cout << "]\n";

  cout << "MinMatrix: " << min << " Index: " << minIndex << endl; // печатает максимальный элемент матрицы

  return 0;
}
