#include <iostream>
#include <string>
using namespace std;

int main(){
  char string[26] = "as kingfishers catch fire";
  int select[5][5];
  for (int i = 0; i < 5; i++){
    for (int j = 0; j < 5; j++){
      if (string[j + (i * 5)] == ' '){
        select[i][j] = '0';
      } else {
        select[i][j] = string[j + (i * 5)];
      }
    }
  }

  for (int i = 0; i < 5; i++){
    for (int j = 0; j < 5; j++){
      select[i][j] = int(select[i][j]) - 96;
      // select[i][j] == -48 ? select[i][j] = 0 : continue;
      if (select[i][j] == -48) select[i][j] = 0;
    }
  }


  for (int i = 0; i < 5; i++){
    cout << "[";
    for (int j = 0; j < 5; j++){
      cout << select[i][j] << ",";
    }
    cout << "]";
  }
  cout << endl;
  // cout.setf(ios::fixed);
  // cout.precision(2);
  // cout << int(select[0][0]) - 96 << endl;
  return 0;
}
