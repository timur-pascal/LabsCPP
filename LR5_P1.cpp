//Вариант 1
#include <iostream>
using namespace std;
int main()
{
    int i=0,j=4;
    // while (i<4){
    //     while (j>0){
    //         cout<<(j>i ? '*' : ' ');
    //         j--;
    //     }
    //     i++;
    //     cout<<endl;
    // }
    while (i<4)
    {
        while (j<4)
        {
            cout<<(j>i ? '*' : ' ')<<" ";
            j++;
        }
        while(j>0)
        {
            cout<<(j>i ? '*' : ' ')<<" ";
            j--;
        }
        i++;
        cout<<endl;
    }
    i=0,j=4;//обнуляет переменные для следующего цикла
    while(i<5)
    {

        while(j>0)
        {
            cout<<(j>i ? ' ' : '*')<<" ";
            j--;
        }
        while(j<5)
        {
            cout<<(j>i ? ' ' : '*')<<" ";
            j++;
        }
        i++;
        cout<<endl;
    }

    cout<<"The programm is end, milord.\n";
    return 0;
}
