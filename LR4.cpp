#include <iostream>
#include <cmath>
using namespace std;
int main(){
    const double eps=0.0001;
    double koef=1,x=2,s=1,slag=2*eps,k=2,znak=-1;
    while(abs(x)>=1){
        cout<<"Input x for -1 in 1=>";
        cin>>x;
    }
    while (abs(slag)>eps){
        koef=k*(k+1)/2;
        slag=znak*koef*pow(x,k-1);
        s+=slag;
        znak=-znak;
        k+=1;
    }
    cout<<"s="<<s<<endl;
    cout<<"Compresion\n"<<s<<"\t"<<1/(pow(1+x,3))<<endl;
    return 0;
}
