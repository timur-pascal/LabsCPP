// Вариант 6
#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int main(){
  const int lenArrow = 10;
  float arrow[lenArrow];
  srand(time(NULL));
  for(int i = 0; i < lenArrow; i++){ // задает массив в 10 элементов
    arrow[i] = (rand() % 10 - 5) * 0.5; // диапозон значений от -10 до 10
    cout<<arrow[i]<<" ";
  }
  float minNumber = arrow[0], firstPositive = -1, lastPsitive = 0;
  float amountElements = 0;
  for(int i = 0; i < lenArrow; i++){
    if (minNumber > arrow[i]){ // находит минимальный элемент массива
      minNumber = arrow[i];
    }
    if (firstPositive == -1 && arrow[i] > 0){ // находит первый положительный элемент массива +1(фикс)
      firstPositive = i+1;
    }
    if (arrow[i] > 0){ // находит последний положительный элемент массива if (lastPsitive == -1 && arrow[i]>0 && lastPsitive == firstPositive)
      lastPsitive = i;
    }
  }
  for (int firstPositive; firstPositive < lastPsitive; firstPositive++){
    amountElements += arrow[firstPositive];
  }
  cout<<endl<<"minNumber =>"<<minNumber<<endl; // выводит минимальный элемент массива
  cout<<"amountElements =>"<<amountElements<<endl; // выводит сумму элементов между первым положительным и последним
  float buffer; //переменная для хранения значения перестановки
  for (int i = 0; i < lenArrow; i++){ // перемещает нули в начало массива
    for (int j = i + 1; j < lenArrow; j++){
      if (arrow[j] == 0){
        for (int i = j; i>0;i--)
        {
          arrow[i]=arrow[i-1];
        }
        arrow[0]=0;
      }
    }
  }
  for (int i = 0; i < lenArrow; i++){ // выводит полученный массив
    cout<<arrow[i]<<" ";
  }
  cout<<endl;
  return 0;
}
