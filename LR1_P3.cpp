#include <iostream>
#include <cmath>
using namespace std;
int main(){
    double x,y,funXY;
    const double a=3,b=4;
    cout<<"Input x,y=>"; cin>>x>>y;
    funXY=((tan(a*(x/y))-(cos(b*(y/x))/sin(b*(y/x))))/a*pow(x,2)+b*pow(y,2))*exp(sqrt(a*x)+sqrt(b*y)));
    cout<<"f(x,y)="<<funXY<<endl;
    return 0;
}
