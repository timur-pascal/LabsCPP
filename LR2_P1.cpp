//Вариант 7
#include <iostream>
#include <cmath>
using namespace std;
int main(){
    double x;
    cout<<"Input x=>"; cin>>x;
    //Первая функция
    if (pow(x,3)==pow(-x,3)) {
        cout<<"Function y1=x3 is even,milord."<<endl;
    }
    else{
        cout<<"Function y1=x^3 is NOT even,milord."<<endl;
    }
    //Вторая функция
    if (pow(x,3)+1==pow(-x,3)+1) {
        cout<<"Function y2=x^3+1 is even,milord."<<endl;
    }
    else{
        cout<<"Function y2=x^3+1 is NOT even,milord."<<endl;
    }
    //Третья функция
    if (1/(1+pow(x,2))==1/(1+pow(-x,2))) {
        cout<<"Function y3=1/(1+x^2) is even,milord."<<endl;
    }
    else{
        cout<<"Function y3=1/(1+x^2) is NOT even,milord."<<endl;
    }
    return 0;
}
