#include <iostream>
using namespace std;
double root(int a)
{
    double s = 0;
    while (a > 0){
        s += a % 10;//считает сумму элементов числа
        a /= 10;//отсекает вычесленный элемент от числа
    }
    if(s > 9){//если сумма имеет два элемента возвращает ее в функцию
        return root(s);
    }
    else {
         return s;//возвращает полученный результат
    }
}
int main(){
    double number;
    cout << "Input number=>"; cin >> number;
    cout << "Root number= " << root(number) << endl;
    return 0;
}
