// Вариант 7
#include <iostream>
using namespace std;

int summNegative(int string, int ** matrix, int size){ // сумма элементов строки
  int summ = 0;
  for(int i = 0; i < size; i++){
    summ += matrix[string][i];
  }
  return summ;
}

void findNegative(int ** matrix, int size){ // поиск отрицательного элемента
  bool find = false;
  for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      if(matrix[i][j] < 0){
        cout << "In string " << i + 1 << " summ = " << summNegative(i, matrix, size);
        find = true;
      }
    }
  }
  if(find == false){
    cout << "Negative is not found\n";
  }
}

void sameString(int ** matrix, int size){ // одинаковые строки и столбцы
  int string;
  for(int i = 0; i < size; i++){
    string = 0;
    for(int j = 0; j < size; j++){
      if(matrix[i][j] == matrix[j][i]){
        string++;
      }
    }
    if(string == size){
      cout << "Number: " << i + 1 << endl;
    }
  }
}

int main(){
  int size;
  int** matrix;
  cout << "Input size matrix: " ;cin >> size;

  matrix = new int*[size];
  for(int i = 0; i < size; i++){
    matrix[i] = new int[size];
  }

  for(int i = 0; i < size; i++){ // ввод матрицы
    for(int j = 0; j < size; j++){
      cout << "Inter element " << "[" << i << "][" << j << "]  ";
      cin >> matrix[i][j];
    }
  }
  for(int i = 0; i < size; i++){ // вывод матрицы
    for(int j = 0; j < size; j++){
      cout << matrix[i][j] << " ";
    }
    cout << endl;
  }

  findNegative(matrix, size);
  sameString(matrix, size);

  for(int i = 0; i < size; i++){ // очищает массив
    delete[]matrix[i];
  }
  return 0;
}
