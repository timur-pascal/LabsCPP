#include <iostream>
#include <cmath>
using namespace std;
double xZero(double a){
    if (a<2){
        return (pow(a,2)+5)/a;
    }
    else if (a>=11){
        return sqrt(a+8/log(a));
    }
    else{
        return 1/(log(pow(a,2)+4))+log(a/5);
    }
}
double sevenNumber(double x,int n){
    if(n>1){
        return 1/(x-1)+sevenNumber(x,--n);
    }
    else{
        return x;
    }
}
int main() {
    double a;
    cout<<"input a=>"; cin>>a;
    cout<<sevenNumber(xZero(a),7)<<endl;
    return 0;
}
