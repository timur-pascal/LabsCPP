#include <iostream>

using namespace std;
int quantitySquare(double a,double b){
    if(a>b){
        return 1+quantitySquare(a-b,b);
    }
    else if(a<b){
        return 1+quantitySquare(a,b-a);
    }
    else{
        return 1;
    }
}
int main(){
    double a,b;
    cout<<"Input a,b=>"; cin>>a>>b;
    cout<<quantitySquare(a,b)<<endl;
    return 0;
}
